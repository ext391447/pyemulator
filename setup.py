from setuptools import setup, find_packages


def run_setup():
    setup(
        name="ZyEmulator",
        version="0.0.1",
        description="Tool for commonize way of emulating cpu architectures. ",
        author="Zyhu",
        author_email="example.com",
        url="https://not_availabe.com",
        packages=find_packages(
            # TODO: remove main.py from this list after moving to tox
            where="src",
            exclude=[
                "tests",
                "docs",
                "roms",
                ".gitignore",
                "README.MD",
                "*.txt",
                "*.pyc",
                "main.py",
            ],
        ),
        package_dir={"": "src"},
        lint_requires=["flake8"],
        install_requires=[
            "Pillow",
        ],
        extras_require={
            "black": ["black~=22.0"],
            "lint": ["flake8"],
            "test": ["pytest"],
        },
    )


if __name__ == "__main__":
    run_setup()
