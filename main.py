from vms.zy80.gbc.gbc import gbc_vm
import os

BASE_PATH = "C:\\Users\\wojci\\zyemulator\\roms\\gbc\\"
PK_YEL_ROM = os.path.join(BASE_PATH, "Pokemon - Yellow Version (UE) [C][!].gbc")
MARIO_DELUXE = os.path.join(BASE_PATH, "Super Mario Bros. Deluxe (U) (V1.1) [C][!].gbc")


def main():
    vm = gbc_vm(rom_path=PK_YEL_ROM)


if __name__ == "__main__":
    main()
