import os

# import warnings
# from shutil import rmtree
# from pathlib import Path

from PIL import Image, ImageDraw


# TODO: refactor
def cleanup_directory(path: str) -> None:
    # assert os.path.exists(path), f"Path {path} does not exists."
    # if os.listdir(path):
    #     warnings.warn(f"Removing contents of filepath: {path}. ")
    #     for path in Path(path).glob("**/*"):
    #         if path.is_file():
    #             path.unlink()
    #         elif path.is_dir():
    #             rmtree(path)

    # assert not os.listdir(path)
    return path


class SpriteViewer:
    GBC_OFFSETS_TILESET = (0x8000, 0x8FFF)
    GBC_SPRITE_BASE_SIZE = 10
    GBC_DEFAULT_SPRITE_SIZE = (8 * GBC_SPRITE_BASE_SIZE, 8 * GBC_SPRITE_BASE_SIZE)
    GBC_SPRITE_PATH = cleanup_directory(
        os.path.join(os.path.dirname(__file__), "output", "sprites")
    )
    # GBC_DEFAULT_SPRITE_SIZE = (16, 16)

    GBC_DEFAULT_COLOR_MAPPING = {
        "00": (255, 255, 255),  # white
        "01": (201, 201, 201),  # light grey
        "10": (82, 82, 82),  # dark grey
        "11": (0, 0, 0),
    }  # black

    def __init__(self, fetched_sprites):
        self._fetched_sprites = fetched_sprites

    def fetch_sprites_from_memory(self, mem_map):
        tilesets = mem_map[slice(*self.GBC_OFFSETS_TILESET)]
        assert len(tilesets) == 4095
        # tilesets = mem_map
        tilesets = mem_map[slice(*(0x150 + 0x8000, 0x150 + 0x8FFF))]

        def fetch_8pixels_line(_rendered_single_line: bytes) -> bytes:
            # 2 bytes represents 8 pixels
            assert (
                len(_rendered_single_line) == 2
            ), "MUST be a 2 bytes per rendered line. "
            _rendered_single_line = list(
                zip(*["{0:0>8b}".format(_) for _ in _rendered_single_line])
            )
            return ["".join(_) for _ in _rendered_single_line]

        def fetch_16pixels_line(_rendered_single_line: bytes) -> bytes:
            # 4 bytes represents 8 pixels
            assert (
                len(_rendered_single_line) == 4
            ), "MUST be a 2 bytes per rendered line. "
            _rendered_single_line_ = list(
                zip(*["{0:0>8b}".format(_) for _ in _rendered_single_line[0:2]])
            )
            _rendered_single_line_following_bytes = list(
                zip(*["{0:0>8b}".format(_) for _ in _rendered_single_line[2:]])
            )

            return [
                "".join(_)
                for _ in _rendered_single_line_ + _rendered_single_line_following_bytes
            ]

        def fetch_32pixels_line(_rendered_single_line: bytes) -> bytes:
            # 4 bytes represents 8 pixels
            assert (
                len(_rendered_single_line) == 8
            ), "MUST be a 2 bytes per rendered line. "
            _rendered_single_line_ = list(
                zip(*["{0:0>8b}".format(_) for _ in _rendered_single_line[0:2]])
            )
            _rendered_single_line_following_bytes = list(
                zip(*["{0:0>8b}".format(_) for _ in _rendered_single_line[2:4]])
            )

            _rendered_single_line_next_ = list(
                zip(*["{0:0>8b}".format(_) for _ in _rendered_single_line[4:6]])
            )
            _rendered_single_line_next__ = list(
                zip(*["{0:0>8b}".format(_) for _ in _rendered_single_line[6:]])
            )

            return [
                "".join(_)
                for _ in _rendered_single_line_
                + _rendered_single_line_following_bytes
                + _rendered_single_line_next_
                + _rendered_single_line_next__
            ]

        image_sprite = Image.new("RGB", self.GBC_DEFAULT_SPRITE_SIZE)
        draw_sprite = ImageDraw.Draw(image_sprite)
        for _chunk_by_sprite_size in range(0, len(tilesets), 16):
            payload = tilesets[
                _chunk_by_sprite_size : _chunk_by_sprite_size + 16  # noqa: E203
            ]
            x_index, y_index = 0, 0
            for i in range(
                0, len(payload), 2
            ):  # TODO: decide if assert above is needed
                try:
                    fetched_line = fetch_8pixels_line(payload[i : i + 2])  # noqa: E203
                except AssertionError:
                    break
                pixels_representation = [
                    self.GBC_DEFAULT_COLOR_MAPPING.get(p) for p in fetched_line
                ]

                x_index = 0
                for color_row in pixels_representation:
                    draw_sprite.rectangle(
                        (
                            x_index,
                            y_index,
                            x_index + self.GBC_SPRITE_BASE_SIZE,
                            y_index + self.GBC_SPRITE_BASE_SIZE,
                        ),
                        fill=color_row,
                        outline=(255, 255, 255),
                    )
                    x_index += self.GBC_SPRITE_BASE_SIZE

                y_index += self.GBC_SPRITE_BASE_SIZE

            image_sprite.save(
                os.path.join(
                    self.GBC_SPRITE_PATH,
                    f"output_sprite_{_chunk_by_sprite_size}_{_chunk_by_sprite_size+16}.jpg",
                ),
                quality=95,
            )  # TODO: use safe save method.
            # TODO: is there clear method?
            # Resetting image.
            image_sprite = Image.new("RGB", self.GBC_DEFAULT_SPRITE_SIZE)
            draw_sprite = ImageDraw.Draw(image_sprite)
        1 == 1

    @classmethod
    def from_memmap(cls, mem_map):
        sprites = cls.fetch_sprites_from_memory(cls, mem_map)
        return cls(sprites)
