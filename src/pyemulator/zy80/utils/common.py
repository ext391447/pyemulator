from logging import INFO
import os
import typing
from dataclasses import (
    dataclass,
)
from time import sleep

from pyemulator.zy80.gbc.utils import SpriteViewer
from pyemulator.zy80.logger import get_logger
from .mappings import z80_mappings


@dataclass
class NoImplemented:
    name: str
    opcode: str


class Z80Stack(bytearray):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # inf: division by two is done to be able access negative address space
        # e.g [esp-4], [esp-8].
        self._stack_ptr = len(self) // 2

    @property
    def stack_pointer(self):
        assert hasattr(self, "_stack_ptr"), "Stack pointer does not exist. "
        return self._stack_ptr

    @stack_pointer.setter
    def stack_pointer(self, val: int):
        assert isinstance(val, int), "Value must be integer. "
        self._stack_ptr = val

    def increment(self) -> None:
        self.stack_pointer += 1

    def decrement(self) -> None:
        self.stack_pointer -= 1

    def mov(self, val: bytes) -> None:
        self[self.stack_pointer] = val

    def fetch(self) -> bytes:
        return self[self.stack_pointer]


# TODO: to be used in future - replace for bytearray stack.
class BinaryStack(list):  # proposition: userlist from collections?
    DATATYPES_MAPPING = {"I": 4, "i": 4, "f": 4, "?": 1, "b": 1, "x": 1}
    STACK_PTR = 0

    # TODO: Future implementation of the stack
    # def validate_data_types(self, *args, **kwargs):
    #     kwargs = list(kwargs)
    #     args = list(args)

    #     for element in chain(kwargs, args):
    #         assert isinstance(element, bytes), f"Wrong type - got: {type(element)} need: bytes. "

    @property
    def stack_pointer(self):
        return self.STACK_PTR

    @stack_pointer.setter
    def stack_pointer(self, val):
        self.STACK_PTR = val

    def __init__(self, *args, **kwargs):
        if args or kwargs:
            raise NotImplementedError
        super().__init__(*args, **kwargs)
        # self.validate_data_types(*args, **kwargs)

    def append(self, val):
        assert isinstance(val, bytes), f"Wrong type - got: {type(val)} need: bytes. "
        size_ = len(val)
        self.stack_pointer += size_
        super().append(val)

    def pop(self):
        ret = super().pop()
        size_ = len(ret)
        if (self.stack_pointer - size_) < 0:
            self._logger.log(
                INFO, f"Pop failed with values: {self.stack_pointer} - {size_} < 0. "
            )
            raise Exception("Data wrongly allocated on the stack. ")

        self.stack_pointer -= size_
        return ret


def validate_path(path: str):
    if os.path.exists(path):
        return path

    raise Exception(f"Path {path} does not exists. ")


# TODO: make a global memory map in terms of inheritance (for each of the emulator)
# TODO: add type-hinting.
class GBMemoryMap:
    @dataclass
    class GBZ80_Flags:
        Z: bool
        N: bool
        H: bool
        C: bool

    @dataclass
    class GBZ80_Registers:
        A: int
        F: int
        H: int
        L: int
        B: int
        C: int
        D: int
        E: int
        SP: int
        PC: int

    accumulator_size = 0x00
    b_register_size = 0x00
    program_counter = 0x00
    # TODO: propably not used
    # stack_pointer = 0x00
    # stack = [] # Refactor: cannot be a list
    # stack = b""  # Refactor: cannot be a list

    def __init__(
        self,
        flags=None,
        registers=None,
        program_counter=program_counter,
        *,
        game_code,
        raw_bytes,
    ):
        self._program_counter = (
            program_counter  # link (getter/setter) to self._registers.A
        )
        self._flags = flags or self.GBZ80_Flags(
            Z=0, N=0, H=0, C=0
        )  # flake8: it is right to use 0 instead False?
        self._registers = registers or self.GBZ80_Registers(
            A=0,
            F=0,
            H=0,
            L=0,
            B=0,
            C=0,
            D=0,
            E=0,
            SP=0,
            PC=0,
        )
        # self._stack = BinaryStack() # TODO: replace in future
        self._stack = Z80Stack(b"\x00" * 0x254)  # TODO: 0x254 should be checked in docs
        # TODO: property for game_code
        self._game_code = game_code
        self._raw_bytes = raw_bytes
        self._logger = get_logger(self)
        1 == 1

    # TODO: RETHINK
    # @property.setter
    # def game_code(self, index, value: int):
    #     assert isinstance(value, int), f"Value: {value} has to be integer. "
    #     self._game_code[]

    @property
    def stack(self):
        assert isinstance(self._stack, Z80Stack), "Stack is not initialized. "
        return self._stack

    def inc_ip(self) -> tuple:  # todo: refactor type-hint
        self.program_counter = self.program_counter[0] + 1
        return self.program_counter

    def __repr__(self):
        lines = [
            "IP",
            str(self.program_counter),
            "REGISTERS",
            str(self._registers),
            "FLAGS",
            str(self.flags),
        ]
        return "\n".join(lines)

    # make a global descriptors for this purpose instead of properties?
    @property
    def program_counter(self):
        assert (
            self._registers.PC is not None
        ), "Program counter/IP cannot be set to None. "
        return self._registers.PC

    @property
    def flags(self):
        assert self._flags is not None, "Flags cannot be set to None. "
        return self._flags

    @program_counter.setter
    def program_counter(self, value):
        assert value is not None, "PC cannot be set to None. "
        self._registers.PC = (value, self._game_code[value])


class zy_80_vm(object):
    NAME_INDEX_ROM = (0xFFFF, 0xFFFF)
    NAME_INDEX_LOGO = (0xFFFF, 0xFFFF)
    GAMEBOY_FLAG = (0xFFFF, 0xFFFF)
    GAMEBOY_CODE = (0xFFFF, 0xFFFF)

    def __init__(self, rom_path):
        self._rom_path = validate_path(rom_path)
        self._rom_bytes = open(self._rom_path, "rb").read()
        self._sprites = SpriteViewer.from_memmap(self._rom_bytes)
        self._opcode_mappings = self.get_opcode_map(z80_mappings)
        self._logger = get_logger(module=self)
        self.base_rom_init()
        self.run()
        1 == 1

    def run(self):
        if self._memory_map.program_counter:
            while True:
                self.handler()
                sleep(0.1)

        raise Exception("Instruction pointer has not been set. ")

    def base_rom_init(self):
        assert self._rom_bytes
        game_name = "".join(
            [chr(self._rom_bytes[i]) for i in range(*self.NAME_INDEX_ROM)]
        )
        assert game_name, str
        # nintendo_logo = [self._rom_bytes[i] for i in range(*self.NAME_INDEX_LOGO)]
        # gameboy_flag = [self._rom_bytes[i] for i in range(*self.GAMEBOY_FLAG)]
        self._current_game = game_name
        self._game_code = [
            (self._rom_bytes[i], hex(self._rom_bytes[i]))
            for i in range(self.GAMEBOY_CODE[0], len(self._rom_bytes))
        ]
        self._memory_map = GBMemoryMap(
            game_code=self._game_code, raw_bytes=self._rom_bytes
        )
        # index, value
        # self._memory_map.program_counter = (0, self._game_code[0])
        self._memory_map.program_counter = 0

    @staticmethod
    def get_opcode_map(mappings):
        # TODO: base validation
        return mappings

    @property
    def memory_map(self):
        assert isinstance(
            self._memory_map, GBMemoryMap
        ), f"Memory map is not a {type(GBMemoryMap)} instance. "
        return self._memory_map

    def handler(self):
        # TODO: ERROR CHECKING, maybe staticmethod?
        # refactor handle method
        assert hasattr(self, "_memory_map"), "MemoryMap has not been set. "
        # TODO: unused - remove?
        ip = self._memory_map.program_counter
        mappings = self._opcode_mappings
        game_code = self._game_code

        def handle(
            ip: tuple,
            mappings: typing.Dict[str, typing.Tuple[typing.Callable, str]],
            game_code: typing.List[typing.Tuple[int, str]],
        ):
            opcode = str(ip[1][1]).replace("0x", "").upper()
            out = mappings.get(
                opcode,
                NoImplemented(f"TODO: Implement {opcode}. ", hex(int(opcode, 16))),
            )
            if isinstance(out, NoImplemented):
                raise NotImplementedError(f"Please implement {out}. ")

            func, _ = out
            self._logger.log(
                INFO,
                f"Handling opcode: 0x{opcode} with function: {func.__name__}. "
                f"Current ip {str(self.memory_map.program_counter)}. ",
            )
            # Address - 3E83 - is RET on emulator handled
            ret = func(self.memory_map)
            if isinstance(ret, Exception):
                raise ret

            self.memory_map.inc_ip()

        handle(ip, mappings, game_code)
