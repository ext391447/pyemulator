import pyemulator.zy80.utils.z80_utils as z80_utils

GAMECODE_ADDRESS = 0x150
# GAMECODE_ADDRESS = 0
z80_mappings = {}


def register_mapping():
    def func_wrapper(func):
        info = func.__doc__
        info = info.replace("\n", "").strip()

        name, opcode = info.split("_")
        z80_mappings[opcode] = (func, name)
        return

    return func_wrapper


def jmp(mem_map):
    e = mem_map.inc_ip()
    value, _ = e[1]  # TODO: refactor - fetching direcrly value from mem_map
    return value


@register_mapping()
def ADC_MAPPING(mem_map):
    """
    ADC_8E
    """
    address = z80_utils.create_16_bit_address(
        mem_map._registers.H, mem_map._registers.L
    )
    address_dereference = mem_map._raw_bytes[address]
    carry_flag = mem_map.flags.C
    mem_map._registers.A += address_dereference + carry_flag


@register_mapping()
def ADC_8A_MAPPING(mem_map):
    """
    ADC_8A
    """
    carry_flag = mem_map.flags.C
    mem_map._registers.A += mem_map._registers.D + carry_flag


@register_mapping()
def ADC_MAPPING_8F():
    """
    ADC_8F
    """
    pass


@register_mapping()
def LDH_MAPPING_78(mem_map):
    """
    LDH_78
    """
    mem_map._registers.A = mem_map._registers.B


@register_mapping()
def RET_NN(mem_map):
    """
    RET_C9
    """
    # Unconditional return from a function.
    lsb, msb = z80_utils.fetch_2bytes_from_stack(mem_map)
    ret_address = z80_utils.unsigned_16(lsb=lsb, msb=msb)
    # start_point = 0x150 # TODO: check if its needed
    mem_map.program_counter = ret_address
    1 == 1


@register_mapping()
def JMP_28(mem_map):
    """
    JR_28
    """

    offset = jmp(mem_map)
    if mem_map.flags.Z != 0:
        mem_map.program_counter = mem_map.program_counter[0] + offset
    1 == 1


@register_mapping()
def LD_FA(mem_map):
    """
    LD_FA
    """
    # Load to the 8-bit A register, data from the absolute
    # address specified by the 16-bit operand nn.
    msb, lsb = z80_utils.get_2bytes_address_from_pc(mem_map)
    address = GAMECODE_ADDRESS + z80_utils.unsigned_16(lsb=lsb, msb=msb)
    mem_map._registers.A = mem_map._game_code[address]
    1 == 1


@register_mapping()
def CATRDIGE_TYPE(mem_map):
    """
    ROM_0
    """
    1 == 1
    pass


@register_mapping()
def JR_E(mem_map):
    """
    JR_18
    """
    (address,) = z80_utils.get_n_bytes_address_from_pc(mem_map, n=1)
    mem_map.program_counter = mem_map.program_counter[0] + address
    1 == 1


@register_mapping()
def CALL_NN(mem_map):
    """
    CALL_CD
    """
    lsb, msb = z80_utils.get_2bytes_address_from_pc(mem_map)
    # address = GAMECODE_ADDRESS + z80_utils.unsigned_16(lsb=lsb, msb=msb)
    address = z80_utils.unsigned_16(lsb=lsb, msb=msb)

    # TODO: commonize by using func that `moving` 2 bytes value onto the stack
    mem_map.stack.decrement()
    _, ret_address = mem_map.program_counter
    ret_address_in_dec, _ = ret_address
    ret_msb, ret_lsb = z80_utils.translate_unsigned_16(ret_address_in_dec)
    mem_map.stack.mov(ret_msb)
    mem_map.stack.decrement()
    mem_map.stack.mov(ret_lsb)
    mem_map.program_counter = address


# Shift memory at HL into carry, LSB is set to 0;
@register_mapping()
def LD_26(mem_map):
    """
    LD_26
    """
    # @https://thomas.spurden.name/gameboy/
    # Load an 8bit immediate value into registry H
    (value,) = z80_utils.get_n_bytes_address_from_pc(mem_map, n=1)
    mem_map._registers.H = value
    1 == 1


@register_mapping()
def LDH_MAPPING(mem_map):
    """
    LDH_F0
    """
    # LDH A, (n): Load accumulator (direct 0xFF00+n)
    index_ = mem_map.inc_ip()[0]  # todo: return other type than self._program_counter
    n = mem_map._game_code[index_][1]
    # TODO: move to common-utils function
    address = n.replace("0x", "")
    address = int(f"0xFF{address}", 16)
    # confirmed: address matches
    mem_map._registers.A = mem_map._game_code[address][0]


# ld ($hl), $reg8
@register_mapping()
def LD_70_MAPPING(mem_map):
    """
    LD_70
    """
    #  {0x70, "LD (HL), B",      &unimplemented_instruction, 8, 1},
    address = z80_utils.create_16_bit_address(
        mem_map._registers.H, mem_map._registers.L
    )
    # TODO: not confirmed if works
    # TODO: refactor - property
    mem_map._game_code[address] = (mem_map._registers.B, hex(mem_map._registers.B))


@register_mapping()
def LD_7D_MAPPING(mem_map):
    """
    LD_7D
    """
    mem_map._registers.A = mem_map._registers.L


@register_mapping()
def LD_62_MAPPING(mem_map):
    """
    LD_62
    """
    mem_map._registers.H = mem_map._registers.D


@register_mapping()
def LD_4D_MAPPING(mem_map):
    """
    LD_4D
    """
    mem_map._registers.C = mem_map._registers.L


@register_mapping()
def LD_7A_MAPPING(mem_map):
    """
    LD_7A
    """
    mem_map._registers.A = mem_map._registers.D


@register_mapping()
def LD_EC_MAPPING(mem_map):
    """
    LD_EC
    """
    # TODO: not implemented
    pass


@register_mapping()
def LD_58_MAPPING(mem_map):
    """
    LD_58
    """
    mem_map._registers.E = mem_map._registers.B


@register_mapping()
def INC_MAPPING(mem_map):
    """
    INC_4
    """
    mem_map._registers.B += z80_utils.inc_8b(mem_map._registers.B)


@register_mapping()
def INC_DE_MAPPING(mem_map):
    """
    INC_13
    """
    value = (
        z80_utils.create_16_bit_value(mem_map._registers.D, mem_map._registers.E) + 1
    )
    mem_map._registers.D, mem_map._registers.E = z80_utils.translate_unsigned_16(value)


@register_mapping()
def LD_59_MAPPING(mem_map):
    """
    LD_59
    """
    mem_map._registers.E = mem_map._registers.C


@register_mapping()
def LD_69_MAPPING(mem_map):
    """
    LD_69
    """
    mem_map._registers.L = mem_map._registers.C


@register_mapping()
def LD_MAPPING(mem_map):
    """
    LD_5A
    """
    mem_map._registers.E = mem_map._registers.D


@register_mapping()
def LDD_3A_MAPPING(mem_map):
    """
    LDD_3A
    """
    address = z80_utils.create_16_bit_address(
        mem_map._registers.H, mem_map._registers.L
    )
    value = mem_map._game_code[address][0]
    mem_map._registers.A = value

    mem_map._registers.H, mem_map._registers.L = z80_utils.update_both_register_value(
        mem_map._registers.H, mem_map._registers.L, -1
    )


@register_mapping()
def LD_5B_MAPPING(mem_map):
    """
    LD_5B
    """
    mem_map._registers.E = (
        mem_map._registers.E
    )  # FIXME: just to not throw any of the exception, pass also may be used


@register_mapping()
def HALT_76_MAPPING(mem_map):
    """
    HALT_76
    """
    # https://www.chibiakumas.com/z80/Gameboy.php
    # The HALT command also has a quirk!, if interrupts are disabled,
    # the command will skip, however the CPU also skips the following command,
    # so put a NOP after HALT.
    pass
    # return Exception(f"HALT has been received: {mem_map!r}. ")


@register_mapping()
def AND_A0_MAPPING(mem_map):
    """
    AND_A0
    """
    value = mem_map._registers.A & mem_map._registers.B
    mem_map._registers.A = value


@register_mapping()
def LD_5D_MAPPING(mem_map):
    """
    LD_5D
    """
    mem_map._registers.E = mem_map._registers.L


@register_mapping()
def LD_5C_MAPPING(mem_map):
    """
    LD_5C
    """
    mem_map._registers.E = mem_map._registers.H


@register_mapping()
def LD_5F_MAPPING(mem_map):
    """
    LD_5F
    """
    mem_map._registers.E = mem_map._registers.A


@register_mapping()
def PUSH_AX_MAPPING(mem_map):
    """
    PUSH_F5
    """
    #  Push register pair AF onto the stack, Decrement Stack Pointer Twice
    mem_map.stack.decrement()
    mem_map.stack.mov(mem_map._registers.F)
    mem_map.stack.decrement()
    mem_map.stack.mov(mem_map._registers.A)


@register_mapping()
def CP_MAPPING(mem_map):
    """
    CP_B8
    """
    a_register = mem_map._registers.A
    b_register = mem_map._registers.B
    result = a_register - b_register
    mem_map.flags.Z = 1 if result == 0 else 0
    mem_map.flags.N = 0
    # TODO: is there better way to achieve following
    # maybe: int(x, 2) was a better use or struct

    def result_to_bits(result: int):
        result_in_bits = "{0:b}".format(result)
        fill_with_zeros = 8 - len(result_in_bits)
        result_fill_to_byte = f"{'0' * fill_with_zeros}{result_in_bits}"

        assert len(result_fill_to_byte) == 8
        return result_fill_to_byte

    carry_bits = result_to_bits(result)
    mem_map.flags.H = 1 if int(carry_bits[3]) else 0
    mem_map.flags.C = 1 if int(carry_bits[7]) else 0
    1 == 1
    # Carry flag
    # 1101< 0101<
