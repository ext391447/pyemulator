def unsigned_16(msb: int, lsb: int) -> int:
    assert msb is not None
    assert lsb is not None
    return int(
        "{msb:08b}{lsb:08b}".format(lsb=lsb, msb=msb), 2
    )  # TODO: could be improved


def inc_8b(value: int) -> int:
    return value + int.from_bytes(b"\x01")


def create_16_bit_address(addr_1, addr_2) -> int:
    # Confirmed: works.
    value = "{0:08b}{1:08b}".format(addr_1, addr_2)
    value = int(value, 2)
    return value


create_16_bit_value = create_16_bit_address


def update_both_register_value(reg_1, reg_2, value: int) -> int:
    value_ = create_16_bit_value(reg_1, reg_2) + value
    return translate_unsigned_16(value_)


def translate_unsigned_16(val: int) -> tuple:
    address = "{0:016b}".format(val)
    msb, lsb = address[:8], address[8:]
    return (int(msb, 2), int(lsb, 2))


def get_n_bytes_address_from_pc(
    mem_map, *, n: int = None
):  # todo: should be used as a replacement for get_2bytes_address_from_pc
    # todo: wrong naming not an address, just value
    assert n is not None, "Bytes number MUST be a integer. "
    values = []
    for _ in range(n):
        index_ = mem_map.inc_ip()
        _, val = index_
        val, _ = val
        assert isinstance(val, int), f"Not an int: {val}. "
        values.append(val)
    return values


def get_2bytes_address_from_pc(mem_map):  # todo: should be implemented in memory map
    # todo: commonize for n-bytes
    # todo: could be done with list comprehension
    values = []
    for _ in range(2):
        index_ = mem_map.inc_ip()
        _, val = index_
        val, _ = val
        assert isinstance(val, int), f"Not an int: {val}. "
        values.append(val)
    return values


def fetch_2bytes_from_stack(mem_map):  # TODO: also refactor
    lsb_msb = []
    for _ in range(2):
        mem_map.stack.increment()
        lsb_msb.append(mem_map.stack.fetch())

    return lsb_msb
