from pyemulator.zy80.utils.common import zy_80_vm


class gbc_vm(zy_80_vm):
    NAME_INDEX_ROM = (0x134, 0x142)
    NAME_INDEX_LOGO = (0x144, 0x145)
    GAMEBOY_FLAG = (0x143, 0x143)
    GAMEBOY_CODE = (0x150, 0xFFFFFFF)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self._sprites = SpriteViewer.from_memmap(self._memory_map._raw_bytes)
        # TODO: move it here from zy_80_vm as its not common
