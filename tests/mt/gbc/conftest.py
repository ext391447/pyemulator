import pytest
import os

ROOT_DIR = os.path.abspath(os.path.dirname(__file__))


@pytest.fixture()
def roms_path():
    return os.path.join(ROOT_DIR, "testdata", "roms")


@pytest.fixture()
def rom_pyellow_path(roms_path):
    assert os.path.exists(roms_path), "roms path does not exist. "
    return os.path.join(roms_path, "Pokemon - Yellow Version (UE) [C][!].gbc")
