import logging


def get_logger(
    module: None,
    formatter: logging.Formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(message)s"
    ),
    file_handler: logging.Handler = logging.FileHandler(
        filename=".pyemulator.log", mode="w"
    ),
) -> logging.Logger:
    assert module is not None, "Please provide class within logger would be used. "
    logger = logging.getLogger(f"{module.__class__.__name__}_logger")
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    logger.log(logging.INFO, f"Logger initialized: {logger!r}. ")
    return logger
