import pytest

from pyemulator.zy80.gbc.vm import gbc_vm


def test_basic():
    assert all([isinstance(x, int) for x in range(10)])


def test_import():
    import pyemulator  # noqa


@pytest.mark.xfail(reason="developement only")
def test_vm(rom_pyellow_path):
    vm = gbc_vm(rom_path=rom_pyellow_path)
    pass


# from vms.zy80.gbc.gbc import gbc_vm
# import os

# BASE_PATH = "C:\\Users\\wojci\\zyemulator\\roms\\gbc\\"
# PK_YEL_ROM = os.path.join(BASE_PATH, "Pokemon - Yellow Version (UE) [C][!].gbc")
# MARIO_DELUXE = os.path.join(BASE_PATH, "Super Mario Bros. Deluxe (U) (V1.1) [C][!].gbc")

# def main():
#     vm = gbc_vm(rom_path=PK_YEL_ROM)


# if __name__ == "__main__":
#     main()
